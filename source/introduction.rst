============
Introduction
============

.. image:: images/archlabs_logo.png
   :height: 200 px
   :width: 200 px
   :alt: ArchLabs Logo
   :align: right

------------------------------
Introduction to ArchLabs Linux
------------------------------

HOW ARCHLABS LINUX CAME TO BE:
==============================

ArchLabs Linux is an Arch Linux based distro, heavily influenced and inspired by the look and feel of **BunsenLabs**.

The more we used BunsenLabs the more we wanted the simplicity that they brought to the table on our Arch install.

After much discussion, **ArchLabs** was born.

We tried to keep the look of BunsenLabs as much as possible in the first months but as we progressed along the way we developed our own style and look.  No longer is ArchLabs a BunsenLabs clone, we are our own beast now.

As we are based on Arch Linux rather than Debian, there are bound to be subtle differences.

Arch Linux is a rolling distro, so is ArchLabs. You will get regular updates from ArchLabs.

WHY A KNOWLEDGE BASE?
=====================

This knowledge base serves the purpose to help the new user out with links to frequently asked questions, wikis for the default applications and utilities as well as provide a base for the user to solve their own issues they may encounter with ArchLabs.  If we can have this information in one place it simplifies searching for your solution.

We understand you can just access any of the applications own wikis or homepages but having all information at one place makes life a whole lot easier.

HOW THE KNOWLEDGE BASE WORKS:
=============================
Like a book the ArchLabs Knowledge Base is broken into chapters.  Simply click the link and you will be directed to that chapter.

CREDITS AND THANK YOUS:
=======================
A lot of this information provided has been taken straight from the Arch Wiki, to the the contributors we thank you.

To the creators, developers and maintainers of the applications and utilities included with ArchLabs, we wouldn't be here without you.
