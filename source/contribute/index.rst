=================
How to Contribute
=================

.. image:: images/archlabs_logo.png
   :height: 286 px
   :width: 286 px
   :alt: ArchLabs logo
   :align: right

-------
Welcome
-------
If you have found your way to this area, most likely there is only one reason you took the trouble even dig this deep into our website. So, first and foremost, welcome and thank you for your interest in contributing to the **ArchLabs Linux Documentation**.
Because, why else would one take such trouble to navigate all the way to our contribution page.

Greetings aside, let's get down to the brass tacks.

GitLab
======
We host our wonderful documentation on *GitLab.* So, as a contributor, we ask that you have a *GitLab* account, to join the project group. Creating a GitLab account is very straight forward and is free. However, you don't need to be apart of the project group to be a contributor.

After you have created the account, you will need to create a forum post on the `ArchLabs Linux Forum <https://forum.archlabslinux.com/>`_. In the post, you will explain what or how you can help contribute to our project. Before submitting the forum post, be sure to include your newly made **GitLab username**, in closing. Once this is complete, our moderators will review it and determine if *ArchLabs* is in need of the additional help at the time you have voiced your interest. And, if there happens to be room for the team to grow, you might get added to the *ArchLabs Group,* on GitLab's site.

How Do I Begin to Contribute?
-----------------------------

Before you begin to contribute to the documentation, there are a few things you should know. Currently the ArchLabs' documentation is written in `reStructuredText <http://github.com/ralsina/rst-cheatsheet/raw/master/rst-cheatsheet.pdf>`_ format, instead of markdown. `RST or reStructuredText <https://github.com/ralsina/rst-cheatsheet/blob/master/rst-cheatsheet.rst>`_ is a fairly simple language that is easy to learn, so I have included to links in the above text, for your basic knowledge. The first link will download a PDF cheat sheet, while the second link will open the cheat sheet in your browser.

You will also need a text editor which supports the '.rst' format. Atom Editor has plugins for this, and makes a solid choice. However, it is not the only editor capable of editing dot 'rst' files. Although, Atom does make it easier to manage larger projects, and other editors might not offer this feature.

One the last things you might want to have for this, is a GUI interface, to help visualize and manage your commits to the repository. The GUI is not necessary, as every thing can be done through the command line and web interface, but should you want to give one a try. A tool like `GitKraken <https://www.gitkraken.com/>`_ is a good choice, and it is free for non commercial works.

reStructuredText Editor Online
``````````````````````````````
If you are not familiar with reStructuredText, don't worry. There is a wonderful `online editor <http://rst.ninjs.org/#>`_ that is very helpful. Are you not confident about your reStructuredText works? Don't know if it will render properly? Before you submit it to the project for inclusion, copy and paste your text into the `online editor <http://rst.ninjs.org/#>`_. This will help ensure your hard work meets the requirements to be included into the project.  You can use a wonderful online tool to check your work.

Now Fork Me!
````````````
So, you have your tools and you are eager to get started. That's great! But, you are not sure if you need to fork the repository or clone it. Well, when working with the documentation we will try to keep this simple.

- *Forking* a project
   - **Fork** to create your own personal repository. Here you can make changes to and test your code before you submit it back upstream.
   - Steps for *forking* can be found in the `GitLab docs <https://docs.gitlab.com/ee/workflow/forking_workflow.html>`_

While working with *ArchLabs Linux Documentation,* please **fork the repository.** As the ArchLabs group grows, our approach may be subject to change.

Diagram of the Work Flow
========================

.. image:: images/docflow.png
   :height: 431 px
   :width: 241 px
   :scale: 80
   :alt: Work Flow
   :align: center

The Work Flow
=============

- Update **Fork**
   - By creating a **pull request**
      - Fork **must be** updated prior to a **merge request**
- Ready to make your first contribution
   - It begins by making a "**commit message**"
      - The **commit** needs a good summary
         - Solid description of changes and additions made to the files
   - Next a **push request** is made
      - The **push** will sync your local and remote repository
   - Now a **merge request** must be made to the *upstream repository*
      - With a **complete summary and description** of the *proposed changes or additions*
- *Upstream* receives the **merge request** for review and processing
   - If the *merge request* is *accepted*, it gets incorporated into the project
   - Should the *merge request* get *rejected*, comments describing our reasoning
      - It is the **your** responsibility to *read the comments*
      - Then *correct* any issue, before you resubmit a *new merge request*

Under Construction
``````````````````
