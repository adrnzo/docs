#####
Tint2
#####

.. image:: images/tin2.png
   :height: 30px
   :width: 1210px
   :scale: 75
   :alt: Tint 2 Status Bar
   :align: center

tint2 is a simple panel/taskbar made for modern X window managers. It was specifically made for Openbox but it should also work with other window managers \(GNOME, KDE, XFCE etc.\). It is based on `ttm <https://code.google.com/p/ttm/>`_ and `ttm archive <https://code.google.com/archive/p/ttm/>`_.

---------
Features:
---------

- Panel with taskbar, system tray, clock and launcher icons;

- Easy to customize: color/transparency on fonts, icons, borders and backgrounds;

- Pager like capability: move tasks between workspaces \(virtual desktops\), switch between workspaces;

- Multi-monitor capability: create one panel per monitor, showing only the tasks from the current monitor;

- Customizable mouse events.

------
Goals:
------

- Be unintrusive and light \(in terms of memory, CPU and aesthetic\);

- Follow the freedesktop.org specifications;

- Make certain workflows, such as multi-desktop and multi-monitor, easy to use.

-------------
Known Issues:
-------------

- Graphic glitches on Intel graphics cards can be avoided by changing the acceleration method to UXA \(issue 595\)

- Window managers that do not follow exactly the EWMH specification might not interact well with tint2 \(known issues for awesome, bspwm. openbox-multihead\)

- Full transparency requires a compositor such as Compton \(if not provided already by the window manager, as in Compiz/Unity, KDE or XFCE\)

----------------
Important Links:
----------------

`Tint2 at Gitlab <https://gitlab.com/o9000/tint2>`_

`Tint2 Wiki <https://gitlab.com/o9000/tint2/wikis/home>`_
