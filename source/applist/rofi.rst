####
Rofi
####

.. image:: images/rofi.png
   :height: 239px
   :width: 798px
   :scale: 75
   :alt: ROFI
   :align: center

**Rofi** is a window switcher, application launcher and dmenu replacement.

**Rofi** started as a clone of simple switcher, written by `Sean Pringle <http://github.com/seanpringle/simpleswitcher>`_ a popup window switcher roughly based on `superswitcher <http://code.google.com/p/superswitcher/>`_. Simpleswitcher laid the foundations, and therefor Sean Pringle deserves most of the credit for this tool. **Rofi** \(renamed, as it lost the _simple_ property\) has been extended with extra features, like an application launcher, ssh-launcher and can act as a drop-in dmenu replacement, making it a very versatile tool.

**Rofi**, like dmenu, will provide the user with a textual list of options where one or more can be selected. This can either be, running an application, selecting a window or options provided by an external script.

Its main features are:

- Fully configurable keyboard navigation
- Type to filter
   - Tokenized: type any word in any order to filter
   - (Togglable) case insensitive
   - Support for fuzzy, regex, and glob matching
- UTF-8 enabled
   - UTF-8-aware string collating
   - International keyboard support (`e -&gt; è)
- RTL language support
- Cairo drawing and Pango font rendering
- Built-in modes:
   - Window switcher mode
   - EWMH compatible WM
   - Application launcher
   - Desktop file application launcher
   - SSH launcher mode
   - Combi mode, allowing several modes to be merged into one list
- History-based ordering — last 25 choices are ordered on top based on use (optional)
- Levenshtein distance ordering of matches (optional)
- Drop-in dmenu replacement
   - Many added improvements
- Easily extensible using scripts
- Theming

**Rofi** has several built-in modes implementing common use cases and can be extended by scripts \(either called from **Rofi** or calling **Rofi**\).
