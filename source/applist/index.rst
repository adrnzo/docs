################
Application List
################

.. toctree::
   :maxdepth: 2

   audacious_player
   baph
   geany
   i3wm
   ob_configurator
   openbox
   pacli
   polybar
   rofi
   skippy_xd
   steam
   termite
   thunar
   tint2
   urxvt
