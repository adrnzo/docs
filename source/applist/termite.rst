#######
Termite
#######

.. image:: images/termite.png
   :height: 399px
   :width:  584px
   :scale: 100
   :alt: Termite Virtual Terminal
   :align: center


`**Termite** <https://www.github.com/thestinger/termite>`_ [1]_ [2]_ is a minimal VTE-based `terminal emulator <https://wiki.archlinux.org/index.php/Category:Terminal_emulators>`_. It is a modal application, similar to `Vim <https://wiki.archlinux.org/index.php/Vim>`_, with an insert mode and selection mode where keybindings have different functions. Termite is based on the `VTE <https://developer.gnome.org/vte/unstable/>`_ library.

The configuration file allows to change colors and set some options. Termite supports transparency along with both the 256 color and true color \(16 million colors\) palettes. It has a similar look and feel to `**urxvt** <https://wiki.archlinux.org/index.php/Urxvt>`_.

-----------
Basic Usage
-----------

Termite starts in insert mode by default. Text may be selected using the mouse, or by using selection-mode keys. In insert mode, `Ctrl+Shift+c` is used to copy selected text to the `X <https://wiki.archlinux.org/index.php/X>`_ clipboard,`Ctrl+Shift+v` to paste.

-------------
Configuration
-------------

Termite looks for configuration files in `$XDG_CONFIG_HOME/termite/config`, `~/.config/termite/config`, `$XDG_CONFIG_DIRS/termite/config` and `/etc/xdg/termite.cfg`. The configuration file is used to change options such as font, colors, window hints, etc. The configuration file is in ini format, with three sections: options, colors, and hints.

To start customizing termite copy the base example file to your home dir first:

``cp /etc/xdg/termite/config ~/.config/termite/config``
-----
Fonts
-----

  Fonts are specified in the format `font=<font_name><font_size>` under the options section. `<font_name>` is specified according to `fontconfig <https://wiki.archlinux.org/index.php/Fontconfig>`_, not `Xft <https://wiki.archlinux.org/index.php/X_Logical_Font_Description>`_. Use `fc-list` to see which fonts are available on the system \(see also `Font configuration\#Font paths <https://wiki.archlinux.org/index.php/Font_configuration#Font_paths>`_.

``~/.config/termite/config``


| [options]
| font = Monospace 9
| font = xos4 Terminus 12px
| font = Droid Sans Mono 8

------
Colors
------

  Colors consist of either a 24-bit hex value \(e.g. `#4a32b1`\), or an rgba vector \(e.g.`rgba(16, 32, 64, 0.5)`\).
  Valid properties for colors are `foreground`, `foreground_bold`, `foreground_dim`, `background`, `cursor`, and `colorN`\(where N is an integer from zero through 254; used to assign a 24-bit color value to terminal colorN\).

``~/.config/termite/config``


| [colors]
| foreground = #dcdccc
| background  = #3f3f3f


------------
Transparency
------------
  Termite supports true transparency via color definitions that specify an alpha channel value. This requires a compositor to be running, such as `Compton <https://wiki.archlinux.org/index.php/Compton>`_ or `xcompmgr <https://www.archlinux.org/packages/?name=xcompmgr>`_. Most compositors do not require special configuration for Termite to use transparency.

``~/.config/termite/config``

| [colors]
| background = rgba(63, 63, 63, 0.8)


-------------------
For I3 Transparency
-------------------

    **Note:** In `i3 <https://wiki.archlinux.org/index.php/I3>`_, in stacked/tabbed layout, this shows all windows "stacked" on top of each other, in the order they were most recently in the foreground, rather than showing the desktop \(the root window\) directly behind Termite. This is due to i3 reordering windows rather than hiding invisible windows in tiling mode. You can configure your compositor to make windows with `_NET_WM_STATE=_NET_WM_STATE_HIDDEN`fully transparent to solve this. For example, for compton use

``~/.compton.conf ``

|    opacity-rule = [
|      "0:_NET_WM_STATE@:32a *= '_NET_WM_STATE_HIDDEN'"
|    ];




.. [1] : `Archwiki <https://www.gnu.org/copyleft/fdl.html>`_ is available under GNU Free Documentation License 1.3 or later unless otherwise noted.
.. [2] : `Termite <https://github.com/thestinger/util/blob/62faf9e46b8c4ab213ac42aaf6343dea9e2dfc1e/LICENSE>`_ is provided under the MIT License, unless otherwise noted.
