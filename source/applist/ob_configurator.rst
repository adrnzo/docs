###############
Ob Configurator
###############

Obconf comes pre-installed on ArchLabs.  Obconf is pretty self explanatory so no detail is required.

.. image:: images/ObConfTheme.png
   :height: 567px
   :width: 422px
   :scale: 75
   :alt: OpenBox Configurator Theme Selections
   :align: left

.. image:: images/ObConfAppearance.png
   :height: 567px
   :width: 422px
   :scale: 75
   :alt: OpenBox Configurator Appearance Settings
   :align: right

.. image:: images/ObConfBehavior.png
   :height: 567px
   :width: 422px
   :scale: 75
   :alt: OpenBox Configurator Behavior Configuration
   :align: left

.. image:: images/ObConfDesktops.png
   :height: 567px
   :width: 422px
   :scale: 75
   :alt: OpenBox Configurator Desktop Configuration
   :align: right

.. image:: images/ObConfDock.png
   :height: 567px
   :width: 422px
   :scale: 75
   :alt: OpenBox Configurator Dock Configuration
   :align: left
