#########
Skippy XD
#########

.. image:: images/skippy-xd4-10.png
   :height: 1050px
   :width: 1680px
   :scale: 50
   :alt: Skippy XD
   :align: center

**Skippy-XD** is a full-screen task-switcher for X11. You know that thing Mac OS X, Compiz and KWin do where you press a hotkey and suddenly you see miniature versions of all your windows at once? **Skippy-XD** does just that. It's most commonly known by Mac OS X's name for it - Exposé.
