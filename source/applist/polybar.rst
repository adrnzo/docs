#######
Polybar
#######


.. image:: images/polybar.png
   :height: 1080px
   :width: 1920px
   :scale: 25
   :alt: Polybar Status Bar
   :align: center


**Polybar** is a fast and easy to use tool for creating status bars.

**Polybar** aims to help users build beautiful and highly customizable status bars for their desktop environment, without the need of having a huge amount of shell scripting knowledge.

**Polybar** uses modules and scripts to display the given information, it makes a more than substantial replacement for conky as a system monitor.  Some of the modules or services included with **Polybar** so far:

- Window title
- Systray icons
- Playback controls and status display for `MPD <https://www.musicpd.org/>`_ using `libmpdclient <https://www.musicpd.org/libs/libmpdclient/>`_
- `ALSA <http://www.alsa-project.org/main/index.php/Main_Page>`_ volume controls
- Workspace and desktop panel for `bspwm <https://github.com/baskerville/bspwm>`_ and `i3 <https://github.com/i3/i3>`_
- Workspace module for `EWMH compliant <https://specifications.freedesktop.org/wm-spec/wm-spec-1.3.html#idm140130320786080>`_ window managers
- Keyboard layout and indicator status
- CPU and memory load indicator
- Battery display
- Network connection details
- Backlight level
- Date and time label
- Time-based shell script execution
- Command output tailing
- User-defined menu tree
- Inter-process messaging
- And more...

Dependencies for **Polybar:**

A compiler with C++14 support `clang-3.4+ <http://llvm.org/releases/download.html>`_, `gcc-5.1+ <https://gcc.gnu.org/releases.html>`_.

- cairo
- libxcb
- python2
- xcb-proto
- xcb-util-image
- xcb-util-wm

Optional dependencies for extended module support:

- alsa-lib _required by _`internal/volume`
- jsoncpp _required by _`internal/i3`
- libmpdclient _required by _`internal/mpd`
- libcurl _required by _`internal/github`
- wireless\_tools _required by _`internal/network`

Find a more complete list on the `dedicated wiki page <https://github.com/jaagr/polybar/wiki/Compiling>`_.

----------------
Running Polybar:
----------------

`See the wiki for details on how to run polybar <https://github.com/jaagr/polybar/wiki>`_.

----------------
Important Links:
----------------

`Github <https://github.com/jaagr/polybar>`_
