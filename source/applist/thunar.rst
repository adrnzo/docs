######
Thunar
######

.. image:: images/thunar.png
   :height: 423px
   :width: 721px
   :scale: 75
   :alt: Thunar File Manager
   :align: center

-------------
INTRODUCTION:
-------------

Thunar [1]_ [2]_ is a modern file manager for the Xfce Desktop Environment. Thunar has been designed from the ground up to be fast and easy to use. Its user interface is clean and intuitive and does not include any confusing or useless options by default. Thunar starts up quickly and navigating through files and folders is fast and responsive.

--------
PLUGINS:
--------

- **The "Send To" Menu**
  Customizing the send to menu.

- **Bulk Renamer**
  Rename multiple files at once.

- **Custom Actions**
  Custom commands associated with common mime-types or extensions.

- **3rd-party**

  - **Archive Plugin**
    This plugin allows users to create and extract archive files using the file context menus.

  - **Media Tags Plugin**
    Extension for the bulk renamer to extract filenames from the ID3 tags and an additional tab in the file properties for ID3 tags information.

  - **Tumbler**
    Tumbler is the remote thumbnail service for Thunar.



.. [1] : content provided by the Thunar wiki is licesned under `CC Attribution-Noncommercial-Share Alike 4.0 International <https://creativecommons.org/licenses/by-nc-sa/4.0/>`_
.. [2] : Documentation for `Thunar <https://docs.xfce.org/xfce/thunar/start>`_
