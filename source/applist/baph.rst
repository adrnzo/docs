#####
baph
#####


**baph** -Basic AUR Package Helper, is an ArchLabs in-house developed AUR helper 

usage: baph <operation> [package(s)] [options]

 operations:
     baph {-h --help}

     baph {-u --update}  [package(s)] [options]

     baph {-i --install} <package(s)> [options]

