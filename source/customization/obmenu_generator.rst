################
Obmenu Generator
################

.. image:: images/ob.png
   :height: 441px
   :width: 526px
   :scale: 100
   :alt: Openbox Desktop Menu
   :align: center

A fast pipe/static menu generator for the Openbox Window Manager, with support for icons.

Obmenu-Generator can be installed from the AUR using "baph" or "yay"

------
Usage:
------

| usage: obmenu-generator [options]
|
| menu:
|     -p         : generate a dynamic menu (pipe)
|     -s         : generate a static menu
|     -i         : include icons
|     -m <id>    : menu id (default: 'root-menu')
|     -t <label> : menu label text (default: 'Applications')
|
| other:
|     -S <file>  : path to the schema.pl file
|     -C <file>  : path to the config.pl file
|     -o <file>  : path to the menu.xml file
|     -u         : update the config file
|     -r         : regenerate the config file
|     -d         : regenerate icons.db
|     -c         : reconfigure openbox automatically
|     -R         : reconfigure openbox and exit
|     -h         : print this message and exit
|     -v         : print version and exit
|
| examples:
|         obmenu-generator -p -i     # dynamic menu with icons
|         obmenu-generator -s -c     # static menu without icons

-----------------------------
Configuration File Locations:
-----------------------------

- **Schema file:**

``~/.config/obmenu-generator/schema.pl``

- **Config file:**

``~/.config/obmenu-generator/config.pl``

----------------
Important Links:
----------------

`Github <https://github.com/trizen/obmenu-generator>`_
