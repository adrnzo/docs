#####
Tint2
#####

.. image:: images/tin2.png
   :height: 30px
   :width: 1210px
   :scale: 75
   :alt: Tint 2 Status Bar
   :align: center


--------------
Configuration:
--------------

tint2 has a configuration file in `~/.config/tint2/tint2rc`. A skeleton configuration file with the default settings is created the first time you run tint2. You can then change this file to your liking. Full documentation on how to configure tint2 is found `here <https://gitlab.com/o9000/tint2/blob/master/doc/tint2.md#configuration>`_.

You can configure the fonts, colors, looks, location and more in this file. The tint2 package also contains a GUI configuration tool that can be launched by running `tint2conf` from Rofi or your favourite launcher.

----------------------
Application Launchers:
----------------------

With version 0.12, it has become possible to add application launchers to tint2. It is necessary to add the following configuration options to your tint2 config file:

Under `#Panel`:

``# Panel``

``panel_items = LTSBC``

And under the new section `#Launchers`:

``#Launchers``

``launcher_icon_theme = LinuxLex-8``

``launcher_padding = 5 0 10``

``launcher_background_id = 9``

``launcher_icon_size = 85``

``launcher_item_app = /some/where/application.desktop``

``launcher_item_app = /some/where/anotherapplication.desktop``


`panel_items` is a new configuration option which defines which items tint2 shows and in what order:

| L
|
| Show Launcher
|
| T
|
| Show Taskbar
|
| S
|
| Show Systray
|
| B
|
| Show Battery status
|
| C
|
| Show Clock
|
| F
|
| Expanded Spacer \(only works without T\)

-----------------------------
Applications Menu in Openbox:
-----------------------------

Since version 0.12, you have the ability to create launchers. Unfortunately, tint2 does not support nested menus yet, so there is no native function to enable an applications menu. This section describes a way to create a launcher for Openbox.

Besides tint2 and Openbox, `install <https://wiki.archlinux.org/index.php/Install>`_ the `xdotool <https://www.archlinux.org/packages/?name=xdotool>`_ package. Next, create a keybinding for opening the Openbox menu. For Openbox, this would require the following entry between the &lt;keyboard&gt; and &lt;/keyboard&gt; tags in `~/.config/openbox/rc.xml`:


| <keybind key="C-A-space">
|  <action name="ShowMenu"><menu>root-menu</menu></action>
| </keybind>

This will set `Ctrl+Alt+Space` to open the root-menu \(this is the menu that opens when you right-click the desktop\). You can change `root-menu` to any menu-id that you have defined in `~/.config/openbox/menu.xml`. Next we need to make that keybinding into a `.desktop` file with `xdotool`. First test that your keybind works with:

``$ xdotool key ctrl+alt+space``

If the menu you chose pops up under your mouse cursor, you have done it right! Now create a `open-openbox-menu.desktop` file inside the `~/.local/share/applications` directory. Add the line `Exec=xdotool key ctrl+alt+space` where `Ctrl+Alt+Space` are your chosen key combinations. Open your new `open-openbox-menu.desktop` file from your file manager and, once again, you should see the menu appear under your cursor. Now just add this to tint2 as a launcher, and you have your Openbox Applications Menu as a launcher for tint2.

------------
Volume Icon:
------------

Tint2 does not come with a volume control applet. We recommend `volume icon. <https://www.archlinux.org/packages/community/x86_64/volumeicon/>`_

--------------------------
Running Tint2 in ArchLabs:
--------------------------

You can run tint2 by simply typing the command:

``$ tint2``

If you want to run tint2 when starting `Openbox <https://wiki.archlinux.org/index.php/Openbox>`_, you will need to edit `~/.config/openbox/autostart` and add the following line:

``tint2 &``

----------------
Multiple Panels:
----------------

Multiple tint2 panels can be simultaneously running by using executing tint2 with different configuration files:


``tint2 -c <path_to_first_config_file>``

``tint2 -c <path_to_second_config_file>``


----------------------
Enabling Transparency:
----------------------

To make tint2 look its best, some form of compositing is required. If your tint2 has a large black rectangular box behind it you are either using a window manager without native compositing \(like Openbox\) or it is not enabled.

To enable compositing under Openbox you can install `Xcompmgr <https://wiki.archlinux.org/index.php/Xcompmgr>`_ or `Cairo Compmgr <https://wiki.archlinux.org/index.php/Cairo_Compmgr>`_, the packages are `xcompmgr <https://www.archlinux.org/packages/?name=xcompmgr>`_, respectively `cairo-compmgr <https://aur.archlinux.org/packages/cairo-compmgr/>`_ AUR.

Xcompmgr can be started like this:

``$ xcompmgr``

You will have to kill and restart tint2 to enable transparency.

If Xcompmgr is used solely to provide tint2 with transparency effects it can be run at boot by changing the autostart section in `~/.config/openbox/autostart` to this:

| # Launch Xcomppmgr and tint2 with openbox
| if which tint2 >/dev/null 2>&1; then
|    (sleep 2 && xcompmgr) &
|    (sleep 2 && tint2) &
| fi


Other \(better\) ways to make Xcompmgr run at startup are discussed in the `Openbox <https://wiki.archlinux.org/index.php/Openbox>`_ article.

-------------------
Fullscreen/Overlay:
-------------------

To force tint2 to stay on top of the application \(overlay\), you need to set the panel\_layer option appropriately. This can be helpful when you switch from a fullscreen window to a normal application using Alt-Tab. There is a discussion on this at `Crunchbang Forum <http://crunchbang.org/forums/viewtopic.php?pid=70048>`_

| #Panel
| panel_layer = top
| strut_policy = follow_size


-----------------------
Third Party Extensions:
-----------------------

It is also possible to extend tint2 with other applications. To add third party extensions, check the "Pages" section in the Official Tint2 Wiki link below.


----------------
Important Links:
----------------

`Tint2 at Gitlab <https://gitlab.com/o9000/tint2>`_

`Tint2 Wiki <https://gitlab.com/o9000/tint2/wikis/home>`_
