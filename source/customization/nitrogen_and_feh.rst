##############
Nitrogen & Feh
##############

Nitrogen is a fast and lightweight desktop background browser and setter for X windows.  It comes as a default application with ArchLabs so installation is not required.

------
Usage:
------

Run `nitrogen --help` for full details. The following examples will get you started:

--------------------
Setting a Wallpaper:
--------------------

To view and set the desired wallpaper from a specific directory recursively, run:

``$ nitrogen /path/to/image/directory/``

To view and set the desired wallpaper from a specific directory non-recursively, run:

``$ nitrogen --no-recurse /path/to/image/directory/``

----------------------
Restoring a Wallpaper:
----------------------
To restore the chosen wallpaper during subsequent sessions, add to your Openbox autostart \(`~.config/openbox/autostart`\)  the following command: `nitrogen --restore &`


Feh
^^^
feh is an X11 image viewer aimed mostly at console users. Unlike most other viewers, it does not have a fancy GUI, but simply displays images. It is controlled via commandline arguments and configurable key/mouse actions.

------------
INSTALLATION
------------

Install feh from Extra.


------
Usage:
------

feh is highly configurable. For a full list of options, run `feh --help` or see the `feh <http://jlk.fjfi.cvut.cz/arch/manpages/man/feh.1>`_ `man page <https://wiki.archlinux.org/index.php/Man_page>`_.

----------------
Browsing Images:
----------------

To quickly browse images in a specific directory, you can launch feh with the following arguments:

``$ feh -g 640x480 -d -S filename /path/to/directory``

- The `-g` flag forces the images to appear no larger than 640x480
- The `-d` flag draws the file name
- The `-S filename` flag sorts the images by file name

This is just one example; there are many more options available should you desire more flexibility.

**Tip:**

The `--start-at` option will display a selected image in feh while allowing to browse all other images in the directory as well, in their default order, i.e. as if you had run "feh \*" and cycled through to the selected image. For example, `feh --start-at ./foo.jpg .` views all images in the current directory, starting with `foo.jpg`.

--------------------
Setting a Wallpaper:
--------------------

feh can be used to set the desktop wallpaper, for example for `window managers <https://wiki.archlinux.org/index.php/Window_manager>`_ without this feature such as `Openbox <https://wiki.archlinux.org/index.php/Openbox>`_, `Fluxbox <https://wiki.archlinux.org/index.php/Fluxbox>`_, and `xmonad <https://wiki.archlinux.org/index.php/Xmonad>`_.

The following command is an example of how to set the initial background:

``$ feh --bg-scale /path/to/image.file``

Other scaling options include:


| --bg-tile FILE
| --bg-center FILE
| --bg-max FILE
| --bg-fill FILE


To restore the background on the next session, add the following to your startup file \(e.g. `~/.xinitrc`, `~/.config/openbox/autostart`, etc.\):

``$ ~/.fehbg &``

To change the background image, edit the file `~/.fehbg` which gets created after running the command `feh --bg-scale /path/to/image.file` mentioned above.

-------------------
Opening SVG Images:
-------------------

``$ feh --magick-timeout 1 file.svg``

Note that this requires the `imagemagick <https://www.archlinux.org/packages/?name=imagemagick>`_ package.

------------------------
Random Background Image:
------------------------

You can have feh set a random wallpaper using the `--randomize` option with one of the `--bg-foo` options, for example:

``$ feh --randomize --bg-fill ~/.wallpaper/*``

The above command tells feh to randomize the list of files in the `~/.wallpaper/` directory and set the backgrounds for all available desktops to whichever images are at the front of the randomized list \(one unique image for each desktop\). You can also do this recursively, if you have your wallpapers divided into subfolders:

``$ feh --recursive --randomize --bg-fill ~/.wallpaper``

To set a different random wallpaper from `~/.wallpaper` each session, add the following to your `.xinitrc`:

``$ feh --bg-max --randomize ~/.wallpaper/* &``

Another way to set a random wallpaper on each x.org session is to edit your `.fehbg` as following.

``$HOME/.fehbg``

``feh --bg-max --randomize --no-fehbg ~/.wallpaper/*``


---------------
Important Links
---------------

`Nitrogen Homepage <http://projects.l3ib.org/nitrogen/>`_

`feh homepage <https://feh.finalrewind.org/>`_
