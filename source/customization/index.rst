#############
Customization
#############

.. toctree::
   :maxdepth: 2

   nitrogen_and_feh
   obmenu_generator
   rofi
   tint2
