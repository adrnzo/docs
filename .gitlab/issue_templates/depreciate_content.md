# Issues Template

## Issue Type
- [ ] Depreciate

## Section Placement
- [ ] Introduction
- [ ] Frequently Asked Questions
- [ ] Application List
- [ ] Customization
- [ ] Depreciated Content
- [ ] Contributions
- [ ] Other

 #### If Other, Explain Here
     ```
     ```

## Proposed Depreciation of Content


## Summary


## Description
