# Issues Template

## Issue Type
- [ ] Change


## Section Placement
- [ ] Introduction
- [ ] Frequently Asked Questions
- [ ] Application List
- [ ] Customization
- [ ] Depreciated Content
- [ ] Contributions
- [ ] Other

 #### If Other, Explain Here
     ```
     ```

## Proposed Change to Content


## Summary


## Description
