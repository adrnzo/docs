# Issues Template

## Issue Type
- [ ] Change


## Select Section
- [ ] Introduction
- [ ] Frequently Asked Questions
- [ ] Application List
- [ ] Customization
- [ ] Depreciated Content
- [ ] Contributions

## Proposed Change Type
- [ ] Name
- [ ] TOC Order
- [ ] Other

  #### If Other, Explain Here
  ```
  ```

## Summary of Proposed Change


## Description
