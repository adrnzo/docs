# Issues Template

## Issue Type
- [ ] New

## Select a Section
- [ ] Introduction
- [ ] Frequently Asked Questions
- [ ] Application List
- [ ] Customization
- [ ] Depreciated Content
- [ ] Contributions

  ### Insert Before or After Selected
     ```
     ```

## Proposed Section Name


## Summary of Contents


## Description of Section
