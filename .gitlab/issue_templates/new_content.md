# Issues Template

## Issue Type
- [ ] New


## Section Placement
- [ ] Introduction
- [ ] Frequently Asked Questions
- [ ] Application List
- [ ] Customization
- [ ] Depreciated Content
- [ ] Contributions
- [ ] Other

 #### If Other, Explain Here
     ```
     ```

## Proposed New Content


## Summary


## Description
